/*
* This proprietary software may be used only as
* authorised by a licensing agreement from ARM Limited
* (C) COPYRIGHT 2009 - 2011 ARM Limited
* ALL RIGHTS RESERVED
* The entire notice above must be reproduced on all authorised
* copies and copies may only be made to the extent permitted
* by a licensing agreement from ARM Limited.
*/

#ifndef SHADER_H
#define SHADER_H

#include <EGL/egl.h>
#include <GLES/gl.h>
#include <GLES2/gl2.h>
#include <jni.h>

struct ShaderObject {
	GLuint vShader;
	GLuint fShader;
	GLuint program;
};

char* load_shader(char *sFilename);
void process_shader_from_file(GLuint * pShader, char *sFilename, GLenum iShaderType);
void process_shader(GLuint *pShader, const char *shader_source, GLenum iShaderType)  ;

#endif

