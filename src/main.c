/*
* Copyright (C) 2010 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/

//BEGIN_INCLUDE(all)
#include <jni.h>
#include <errno.h>

#include <EGL/egl.h>
//#include <GLES/gl.h>
#include <GLES2/gl2.h>

#include <android/sensor.h>
#include "strings.h"
#include <android/log.h>
#include <android_native_app_glue.h>
#include "../include/matrix.h"
#include "../include/shader.h"
#include "app_help.c"
#include "vertex_arrays.c"


/* 3D data. Vertex range -0.5..0.5 in all axes.
* Z -0.5 is near, 0.5 is far. */
const float aVertices[] =
{
	/* Front face. */
	/* Bottom left */
	-0.5,  0.5, -0.5,
	0.5, -0.5, -0.5,
	-0.5, -0.5, -0.5,
	/* Top right */
	-0.5,  0.5, -0.5,
	0.5,  0.5, -0.5,
	0.5, -0.5, -0.5,
	/* Left face */
	/* Bottom left */
	-0.5,  0.5,  0.5,
	-0.5, -0.5, -0.5,
	-0.5, -0.5,  0.5,
	/* Top right */
	-0.5,  0.5,  0.5,
	-0.5,  0.5, -0.5,
	-0.5, -0.5, -0.5,
	/* Top face */
	/* Bottom left */
	-0.5,  0.5,  0.5,
	0.5,  0.5, -0.5,
	-0.5,  0.5, -0.5,
	/* Top right */
	-0.5,  0.5,  0.5,
	0.5,  0.5,  0.5,
	0.5,  0.5, -0.5,
	/* Right face */
	/* Bottom left */
	0.5,  0.5, -0.5,
	0.5, -0.5,  0.5,
	0.5, -0.5, -0.5,
	/* Top right */
	0.5,  0.5, -0.5,
	0.5,  0.5,  0.5,
	0.5, -0.5,  0.5,
	/* Back face */
	/* Bottom left */
	0.5,  0.5,  0.5,
	-0.5, -0.5,  0.5,
	0.5, -0.5,  0.5,
	/* Top right */
	0.5,  0.5,  0.5,
	-0.5,  0.5,  0.5,
	-0.5, -0.5,  0.5,
	/* Bottom face */
	/* Bottom left */
	-0.5, -0.5, -0.5,
	0.5, -0.5,  0.5,
	-0.5, -0.5,  0.5,
	/* Top right */
	-0.5, -0.5, -0.5,
	0.5, -0.5, -0.5,
	0.5, -0.5,  0.5,
};

const float aColours[] =
{
	/* Front face */
	/* Bottom left */
	1.0, 0.0, 0.0, /* red */
	0.0, 0.0, 1.0, /* blue */
	0.0, 1.0, 0.0, /* green */
	/* Top right */
	1.0, 0.0, 0.0, /* red */
	1.0, 1.0, 0.0, /* yellow */
	0.0, 0.0, 1.0, /* blue */
	/* Left face */
	/* Bottom left */
	1.0, 1.0, 1.0, /* white */
	0.0, 1.0, 0.0, /* green */
	0.0, 1.0, 1.0, /* cyan */
	/* Top right */
	1.0, 1.0, 1.0, /* white */
	1.0, 0.0, 0.0, /* red */
	0.0, 1.0, 0.0, /* green */
	/* Top face */
	/* Bottom left */
	1.0, 1.0, 1.0, /* white */
	1.0, 1.0, 0.0, /* yellow */
	1.0, 0.0, 0.0, /* red */
	/* Top right */
	1.0, 1.0, 1.0, /* white */
	0.0, 0.0, 0.0, /* black */
	1.0, 1.0, 0.0, /* yellow */
	/* Right face */
	/* Bottom left */
	1.0, 1.0, 0.0, /* yellow */
	1.0, 0.0, 1.0, /* magenta */
	0.0, 0.0, 1.0, /* blue */
	/* Top right */
	1.0, 1.0, 0.0, /* yellow */
	0.0, 0.0, 0.0, /* black */
	1.0, 0.0, 1.0, /* magenta */
	/* Back face */
	/* Bottom left */
	0.0, 0.0, 0.0, /* black */
	0.0, 1.0, 1.0, /* cyan */
	1.0, 0.0, 1.0, /* magenta */
	/* Top right */
	0.0, 0.0, 0.0, /* black */
	1.0, 1.0, 1.0, /* white */
	0.0, 1.0, 1.0, /* cyan */
	/* Bottom face */
	/* Bottom left */
	0.0, 1.0, 0.0, /* green */
	1.0, 0.0, 1.0, /* magenta */
	0.0, 1.0, 1.0, /* cyan */
	/* Top right */
	0.0, 1.0, 0.0, /* green */
	0.0, 0.0, 1.0, /* blue */
	1.0, 0.0, 1.0, /* magenta */
};

float aRotate[16], aModelView[16], aPerspective[16], aMVP[16];


GLboolean is_in_action;


//float aRotate[16], aModelView[16], aPerspective[16], aMVP[16];
//int iXangle = 0, iYangle = 0, iZangle = 0;
//GLint iLocColour, iLocTexCoord, iLocNormal, iLocMVP;

/**
* Initialize an EGL context for the current display.
*/
static int engine_init_display(struct engine* engine) {
	// initialize OpenGL ES and EGL

	is_in_action = GL_FALSE;

	/*
	* Here specify the attributes of the desired configuration.
	* Below, we select an EGLConfig with at least 8 bits per color
	* component compatible with on-screen windows
	*/
	const EGLint attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_BLUE_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_RED_SIZE, 8,
		EGL_NONE
	};
	EGLint w, h, dummy, format;
	EGLint numConfigs;
	EGLConfig config;
	EGLSurface surface;
	EGLContext context;

	EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	eglInitialize(display, 0, 0);

	/* Here, the application chooses the configuration it desires. In this
	* sample, we have a very simplified selection process, where we pick
	* the first EGLConfig that matches our criteria */
	eglChooseConfig(display, attribs, &config, 1, &numConfigs);

	/* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
	* guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
	* As soon as we picked a EGLConfig, we can safely reconfigure the
	* ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
	eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
	ANativeWindow_setBuffersGeometry(engine->app->window, 0, 0, format);

	surface = eglCreateWindowSurface(display, config, (EGLNativeWindowType) engine->app->window, NULL);

	EGLint contextAttrs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	context = eglCreateContext(display, config, NULL, contextAttrs);

	if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
		LOGW("Unable to eglMakeCurrent");
		return -1;
	}
	eglQuerySurface(display, surface, EGL_WIDTH, &w);
	eglQuerySurface(display, surface, EGL_HEIGHT, &h); 
	engine->display = display;
	engine->context = context;
	engine->surface = surface;
	engine->width = w;
	engine->height = h;
	engine->state.x_axis_angle = 0;
	engine->state.y_axis_angle = 0;
	engine->shaderObject = malloc(sizeof(struct ShaderObject))
		GL_CHECK();
	EGL_CHECK();
	// Initialize GL state.
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	//glViewport(0,0,w,h);

	return 0;
}

static void enable_shaders(struct engine* engine) {
	const GLchar *v1 = {
		"attribute vec4 av4position;\n" 
		"attribute vec3 av3colour;\n" 
		"\n" 
		"uniform mat4 mvp;\n" 
		"\n" 
		"varying vec3 vv3colour;\n" 
		"\n" 
		"void main() {\n" 
		"\tvv3colour = av3colour;\n" 
		"\tvec4 position = vec4(av4position.xyz, 1.0);\n"
		"\tgl_Position = mvp * position;\n" 
		"}\n"
	};

	const GLchar *v2 = {
		"precision lowp float;\n" 
		"\n" 
		"varying vec3 vv3colour;\n" 
		"\n" 
		"void main() {\n" 
		"\tgl_FragColor = vec4(vv3colour, 1.0);\n" 
		"}"
	};
	LOGI("asffafagaf");
	GLuint fShader;
	GLuint vShader;
	GLuint program;
	/* Shader Initialisation */
	process_shader(&vShader, v1, GL_VERTEX_SHADER);
	process_shader(&fShader, v2, GL_FRAGMENT_SHADER);
	engine->shaderObject->vShader = vShader;
	engine->shaderObject->fShader = fShader;
	LOGI("asffafagaf1, %i", engine->shaderObject->vShader);

	/* Create uiProgram (ready to attach shaders) */
	engine->shaderObject->program = glCreateProgram();
	LOGI("asffafagaf2, %i", engine->shaderObject->program);
	/* Attach shaders and link uiProgram */
	glAttachShader(engine->shaderObject->program, engine->shaderObject->fShader);
	GL_CHECK();
	glAttachShader(engine->shaderObject->program, engine->shaderObject->vShader);
	GL_CHECK();
	glLinkProgram(engine->shaderObject->program);
	GL_CHECK();
	LOGI("asffafagaf3");

	/* Get attribute locations of non-fixed attributes like colour and texture coordinates. */
	engine->positionLocation = GL_CHECK(glGetAttribLocation(engine->shaderObject->program, "av4position"));
	engine->colorLocation = GL_CHECK(glGetAttribLocation(engine->shaderObject->program, "av3colour"));
	LOGI("asffafagaf4");


#ifdef DEBUG
	printf("iLocPosition = %i\n", iLocPosition);
	printf("iLocColour   = %i\n", iLocColour);
#endif

	/* Get uniform locations */
	engine->mvpLocation = GL_CHECK(glGetUniformLocation(engine->shaderObject->program, "mvp"));

#ifdef DEBUG
	printf("iLocMVP      = %i\n", iLocMVP);
#endif
	LOGI("Program object %i %i %i %i", engine->mvpLocation, 
		engine->shaderObject->fShader, engine->shaderObject->vShader, engine->shaderObject->program);
	GL_CHECK(glUseProgram(engine->shaderObject->program));

	/* Enable attributes for position, colour and texture coordinates etc. */
	GL_CHECK(glEnableVertexAttribArray(engine->positionLocation));
	GL_CHECK(glEnableVertexAttribArray(engine->colorLocation));

	/* Populate attributes for position, colour and texture coordinates etc. */
	GL_CHECK(glVertexAttribPointer(engine->positionLocation, 3, GL_FLOAT, GL_FALSE, 0, aVertices));
	GL_CHECK(glVertexAttribPointer(engine->colorLocation, 3, GL_FLOAT, GL_FALSE, 0, aColours));

	LOGI("asffafagaf");

	//GL_CHECK(glEnable(GL_CULL_FACE));
	//GL_CHECK(glEnable(GL_DEPTH_TEST));
}

/**
* Just the current frame in the display.
*/
static void engine_draw_frame(struct engine* engine) {
	if (engine->display == NULL) {
		// No display.
		return;
	}

	/* 
	* Do some rotation with Euler angles. It is not a fixed axis as
	* quaterions would be, but the effect is cool. 
	*/
	rotate_matrix(0, 0.0, 0.0, 1.0, aModelView);
	rotate_matrix(engine->state.x_axis_angle, 0.0, 1.0, 0.0, aRotate);

	multiply_matrix(aRotate, aModelView, aModelView);

	rotate_matrix(engine->state.y_axis_angle, 1.0, 0.0, 0.0, aRotate);

	multiply_matrix(aRotate, aModelView, aModelView);

	/* Pull the camera back from the cube */
	aModelView[14] -= 2.5;

	perspective_matrix(45.0, (double)engine->width/(double)engine->height, 0.01, 100.0, aPerspective);
	// orthographic_matrix(0.0, (double)engine->width, (double)engine->height, 0.0, 0.01, 100.0, aPerspective);
	multiply_matrix(aPerspective, aModelView, aMVP);

	//float aMVP[16] = { 
	//	2.0/engine->height, 0.0, 0.0, -1.0,
	//	0.0, 2.0/engine->width, 0.0, -1.0,
	//	0.0, 0.0, -1.0, 0.0,
	//	0.0, 0.0, 0.0, 1.0
	//};

	GL_CHECK(glUniformMatrix4fv(engine->mvpLocation, 1, GL_FALSE, aMVP));

	//iXangle += 3;
	//iYangle += 2;
	//iZangle += 1;

	//if(iXangle >= 360) iXangle -= 360;
	//if(iXangle < 0) iXangle += 360;
	//if(iYangle >= 360) iYangle -= 360;
	//if(iYangle < 0) iYangle += 360;
	//if(iZangle >= 360) iZangle -= 360;
	//if(iZangle < 0) iZangle += 360;

	GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
	GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, 36));

	if (!eglSwapBuffers(engine->display, engine->surface)) {
		LOGI("Failed to swap buffers.\n");
	}
}

/**
* Tear down the EGL context currently associated with the display.
*/
static void engine_term_display(struct engine* engine) {
	if (engine->display != EGL_NO_DISPLAY) {
		eglMakeCurrent(engine->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		if (engine->context != EGL_NO_CONTEXT) {
			eglDestroyContext(engine->display, engine->context);
		}
		if (engine->surface != EGL_NO_SURFACE) {
			eglDestroySurface(engine->display, engine->surface);
		}
		eglTerminate(engine->display);
	}
	engine->animating = 0;
	engine->display = EGL_NO_DISPLAY;
	engine->context = EGL_NO_CONTEXT;
	engine->surface = EGL_NO_SURFACE;
}

/**
* Process the next input event.
*/
static int32_t engine_handle_input(struct android_app* app, AInputEvent* event) {
	struct engine* engine = (struct engine*)app->userData;

	if(AMotionEvent_getAction(event) == AMOTION_EVENT_ACTION_UP)
	{
		engine->state.last_x = 0;
		engine->state.last_y = 0;
		is_in_action = GL_FALSE;
		LOGI("AMOTION_EVENT_ACTION_UP");
	}
	if(AMotionEvent_getAction(event) == AMOTION_EVENT_ACTION_OUTSIDE)
	{
		is_in_action = GL_FALSE;
		LOGI("AMOTION_EVENT_ACTION_OUTSIDE");
	}
	if(AMotionEvent_getAction(event) == AMOTION_EVENT_ACTION_CANCEL)
	{
		is_in_action = GL_FALSE;
		LOGI("AMOTION_EVENT_ACTION_CANCEL");
	}
	if(AMotionEvent_getAction(event) == AMOTION_EVENT_ACTION_DOWN)
	{
		is_in_action = GL_TRUE;
		LOGI("AMOTION_EVENT_ACTION_DOWN");
	}

	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION && is_in_action == GL_TRUE) {

		engine->animating = 1;

		engine->state.x = AMotionEvent_getX(event, 0);
		engine->state.y = AMotionEvent_getY(event, 0);

		if(engine->state.last_x != 0)
		{
			engine->state.x_axis_angle += (engine->state.x - engine->state.last_x);
			engine->state.y_axis_angle += (engine->state.y - engine->state.last_y);
		}
		/*if(engine->state.last_x == 0)
		{
		engine->state.x_axis_angle = 0;
		}
		if(engine->state.last_y == 0)
		{
		engine->state.y_axis_angle = 0;
		}*/

		if(engine->state.y_axis_angle >= 360) engine->state.y_axis_angle -= 360;
		if(engine->state.y_axis_angle < 0) engine->state.y_axis_angle += 360;

		if(engine->state.x_axis_angle >= 360) engine->state.x_axis_angle -= 360;
		if(engine->state.x_axis_angle < 0) engine->state.x_axis_angle += 360;

		char * result = (char *) malloc(sizeof(char)*32);
		sprintf(result, "motion event, %d, %d", engine->state.x, engine->state.last_x); 

		engine->state.last_x = engine->state.x;
		engine->state.last_y = engine->state.y;

		//LOGI(result);

		free(result);

		return 1;
	}

	return 0;
}

/**
* Process the next main command.
*/
static void engine_handle_cmd(struct android_app* app, int32_t cmd) {
	struct engine* engine = (struct engine*)app->userData;
	switch (cmd) {
	case APP_CMD_SAVE_STATE:
		// The system has asked us to save our current state.  Do so.
		engine->app->savedState = malloc(sizeof(struct saved_state));
		*((struct saved_state*)engine->app->savedState) = engine->state;
		engine->app->savedStateSize = sizeof(struct saved_state);
		break;
	case APP_CMD_INIT_WINDOW:
		// The window is being shown, get it ready.
		if (engine->app->window != NULL) {
			engine_init_display(engine);
			enable_shaders(engine);
			engine_draw_frame(engine);
		}
		break;
	case APP_CMD_TERM_WINDOW:
		// The window is being hidden or closed, clean it up.
		engine_term_display(engine);
		break;
	case APP_CMD_GAINED_FOCUS:
		// When our app gains focus, we start monitoring the accelerometer.
		if (engine->accelerometerSensor != NULL) {
			ASensorEventQueue_enableSensor(engine->sensorEventQueue,
				engine->accelerometerSensor);
			// We'd like to get 60 events per second (in us).
			ASensorEventQueue_setEventRate(engine->sensorEventQueue,
				engine->accelerometerSensor, (1000L/60)*1000);
		}
		break;
	case APP_CMD_LOST_FOCUS:
		// When our app loses focus, we stop monitoring the accelerometer.
		// This is to avoid consuming battery while not being used.
		if (engine->accelerometerSensor != NULL) {
			ASensorEventQueue_disableSensor(engine->sensorEventQueue,
				engine->accelerometerSensor);
		}
		// Also stop animating.
		engine->animating = 0;
		engine_draw_frame(engine);
		break;
	}
}


/**
* This is the main entry point of a native application that is using
* android_native_app_glue.  It runs in its own thread, with its own
* event loop for receiving input events and doing other things.
*/
void android_main(struct android_app* state) {
	struct engine engine;

	// Make sure glue isn't stripped.
	app_dummy();

	memset(&engine, 0, sizeof(engine));
	state->userData = &engine;
	state->onAppCmd = engine_handle_cmd;
	state->onInputEvent = engine_handle_input;
	engine.app = state;

	// Prepare to monitor accelerometer
	engine.sensorManager = ASensorManager_getInstance();
	engine.accelerometerSensor = ASensorManager_getDefaultSensor(engine.sensorManager,
		ASENSOR_TYPE_ACCELEROMETER);
	engine.sensorEventQueue = ASensorManager_createEventQueue(engine.sensorManager,
		state->looper, LOOPER_ID_USER, NULL, NULL);

	if (state->savedState != NULL) {
		// We are starting with a previous saved state; restore from it.
		engine.state = *(struct saved_state*)state->savedState;
	}

	// loop waiting for stuff to do.

	while (1) {
		// Read all pending events.
		int ident;
		int events;
		struct android_poll_source* source;

		// If not animating, we will block forever waiting for events.
		// If animating, we loop until all events are read, then continue
		// to draw the next frame of animation.
		while ((ident=ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events,
			(void**)&source)) >= 0) {

				// Process this event.
				if (source != NULL) {
					source->process(state, source);
				}

				// If a sensor has data, process it now.
				if (ident == LOOPER_ID_USER) {
					if (engine.accelerometerSensor != NULL) {
						ASensorEvent event;
						while (ASensorEventQueue_getEvents(engine.sensorEventQueue,
							&event, 1) > 0) {

						}
					}
				}

				// Check if we are exiting.
				if (state->destroyRequested != 0) {
					engine_term_display(&engine);
					return;
				}
		}

		if (engine.animating) {
			//LOGI("draw frame\n");

			// Drawing is throttled to the screen update rate, so there
			// is no need to do timing here.
			engine_draw_frame(&engine);
		}
	}
}
//END_INCLUDE(all)
