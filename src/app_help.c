#ifndef APPHELP
#define APPHELP

//BEGIN_INCLUDE(all)
#include <jni.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <EGL/egl.h>
#include <GLES/gl.h>
#include <GLES2/gl2.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include "../include/matrix.h"


typedef struct {
	float x, y, z;
} vec3;


#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "native-activity", __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "native-activity", __VA_ARGS__))

#define CPSTR(x,y) \
{\
	char * bufSecs = new char[32];\
	sprintf(bufSecs,"%d",y);\
	return bufSecs;\
}

#define GL_CHECK(x) \
	x; \
{ \
	GLenum glError = glGetError(); \
	if(glError != GL_NO_ERROR) { \
	LOGE("glGetError() = %i (0x%.4x) in file %s at line %i\n", glError, glError, __FILE__, __LINE__); \
	exit(1); \
	} \
}

#define EGL_CHECK(x) \
	x; \
{ \
	EGLint eglError = eglGetError(); \
	if(eglError != EGL_SUCCESS) { \
	LOGE("eglGetError() = %i (0x%.4x) in file %s at line %i\n", eglError, eglError,  __FILE__, __LINE__); \
	exit(1); \
	} \
}


/**
* Our saved state data.
*/
struct saved_state {
	float y_axis_angle;
	float x_axis_angle;
	int32_t x;
	int32_t y;

	int32_t last_x;
	int32_t last_y;
};

/**
* Shared state for our app.
*/
struct engine {
	struct android_app* app;

	ASensorManager* sensorManager;
	const ASensor* accelerometerSensor;
	ASensorEventQueue* sensorEventQueue;

	int animating;
	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;
	int32_t width;
	int32_t height;
	struct saved_state state;
	struct ShaderObject* shaderObject;

	GLuint colorLocation;
	GLuint positionLocation;
	GLuint mvpLocation;
};

#endif